# PH Covid 19 Screener Firebase Functions


# Instructions

# Install firebase tools

```bash
npm install -g firebase-tools
```

Go to the directory where you cloned this repo.
Make sure you are logged in firebase console:

```bash
firebase login
```

# Running firebase locally

It is recommended to test your Firebase Functions before deploying the codes. 
To test, run firebase functions locally:

```bash
firebase serve --only functions
```

You can use PostMan to send POST/GET requests.

# Deploying

```bash
firebase deploy --only functions
```

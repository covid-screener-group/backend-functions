const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const app = express();

//allow x-origin requests for now
app.use(cors({origin: true}));


app.post("/assess", (request, response) => {
  let hasSymptoms, isSymptomSevere, hadExposureHistory, isElderly, comorbidityPresent, assessment;

  let exposureHistoryRelatedAnswers = [];

  let items = request.body;
  //just to check incoming data
  console.log(items);
  
  //loop through items and extract their records
  items.forEach((item, index) => {
    let answer = item.answer;
    switch(item.question){
      case 'symptoms':
        hasSymptoms = answer.length > 0;
      break;
      case 'severity':
        isSymptomSevere = answer === 'Yes';
      break;
      case 'travel':
      case 'closeContact':
      case 'areaWithCases':
      case 'increaseFlueCases':      
        if(answer === 'Yes'){
          exposureHistoryRelatedAnswers.push(answer);  
        }        
      break;
      case 'ageGroup':
        isElderly = answer === '60 years old and above';
      break;
      case 'conditions':
        comorbidityPresent = answer.length > 0;
      break;
    }
  });


  hadExposureHistory = exposureHistoryRelatedAnswers.length > 0;

  //decision tree
  if(hasSymptoms){
    if(isSymptomSevere){
      assessment = 'PUI-B'; //hasSymptoms and isSymptomsSevere
    }else{
      assessment = 'NPUMI'; // default answer if hasSymptoms but hadExposureHistory = false
      if(hadExposureHistory){
        if(isElderly){
          assessment = 'PUI-B'; //hasSymptoms, !isSymptomsSevere, hadExposureHistory, isElderly 
        }else{
          assessment = 'PUI-A'; //hasSymptoms, !isSymptomsSevere, hadExposureHistory
        }
      }
    }
  }else{
    if(hadExposureHistory){ // !hasSymptoms. hadExposureHistory
      assessment = 'PUM';
    }
  }

  response.json({
    success: true,
    assessment: assessment
  });
});

exports.self_check = functions.https.onRequest(app);